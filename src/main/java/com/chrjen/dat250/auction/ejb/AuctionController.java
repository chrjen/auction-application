package com.chrjen.dat250.auction.ejb;

import com.chrjen.dat250.auction.entities.Auction;
import com.chrjen.dat250.auction.entities.Bid;
import com.chrjen.dat250.auction.util.StringParse;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@ManagedBean
@Named("auctionController")
@RequestScoped
public class AuctionController {

	public static final int NUM_AUCTION_PER_PAGE = 12;
	public static final int MAX_DESC_SIZE = 120;
	public static final int RECENT_BID_SIZE = 6;

	private static final SimpleDateFormat dateFormat =
			new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@EJB
	private AuctionDao auctionDao;

	private String bidAmount;


	@SuppressWarnings("unused")
	public List<Auction> getDisplayAuctions() {

		int start = (getCurrentPage() - 1) * NUM_AUCTION_PER_PAGE;

		if (start < 0) {
			start = 0;
		}

		return auctionDao.getActiveAuctions(start, NUM_AUCTION_PER_PAGE);
	}


	@SuppressWarnings("unused")
	public Auction getAuction() {

		return auctionDao.getAuction(getCurrentId());
	}


	@SuppressWarnings("unused")
	public String truncateDescription(String description) {

		if (description.length() > MAX_DESC_SIZE) {
			description = description.substring(0, MAX_DESC_SIZE) + '\u2026';
		}

		return description;
	}


	@SuppressWarnings("unused")
	public String formatPrice(int amount) {

			NumberFormat format = NumberFormat.getCurrencyInstance(Locale.FRANCE);
			return format.format(amount / 100d).replace(' ', '\u00a0');
	}


	@SuppressWarnings("unused")
	public String formatTime(long time) {

		return dateFormat.format(new Date(time));
	}


	@SuppressWarnings("unused")
	public int getLastPage() {

		return 1 + auctionDao.getActiveAuctionsSize() / NUM_AUCTION_PER_PAGE;
	}


	public int getCurrentId() {

		Map<String, String> params = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap();
		return StringParse.getIntOrDefault(params.get("id"), 1);
	}


	public int getCurrentPage() {

		Map<String, String> params = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap();
		return StringParse.getIntOrDefault(params.get("p"), 1);
	}


	@SuppressWarnings("unchecked")
	public String makeBid() {

		int amount = Integer.parseInt(bidAmount.replace(",", "").replace(".", ""));

		Bid bid = new Bid(amount, System.currentTimeMillis());

		if (!auctionDao.addBid(getAuction(), bid)) {

			// Failed to make bid
			return "product?=" + getCurrentId();
		}

		auctionDao.persist(bid);

		return "product?=" + getCurrentId();
	}


	@SuppressWarnings("unused")
	public List<Bid> getRecentBids(int id) {

		return auctionDao.getMostRecent(auctionDao.getAuction(id), RECENT_BID_SIZE);
	}


	public String getBidAmount() {

		return bidAmount;
	}


	public void setBidAmount(String bidAmount) {

		this.bidAmount = bidAmount;
	}
}
