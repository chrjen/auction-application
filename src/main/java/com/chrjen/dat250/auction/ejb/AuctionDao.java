package com.chrjen.dat250.auction.ejb;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSSessionMode;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.chrjen.dat250.auction.entities.Auction;
import com.chrjen.dat250.auction.entities.Bid;


@Stateless
@SuppressWarnings("WeakerAccess")
public class AuctionDao {

	@PersistenceContext(unitName="AuctionPU")
	private EntityManager em;

	@Inject
	@JMSConnectionFactory("jms/dat250/ConnectionFactory")
	@JMSSessionMode(JMSContext.AUTO_ACKNOWLEDGE)
	private JMSContext context;

	@Resource(lookup = "jms/dat250/Topic")
	private Topic topic;

	Logger logger = Logger.getLogger(getClass().getName());

	public void persist(Auction auction) {

		em.persist(auction);
	}


	public void persist(Bid bid) {

		em.persist(bid);

	}


	public Auction getAuction(int id) {

		return em.find(Auction.class, id);
	}


	/**
	 * Add new bid to auction. It is not added if
	 *
	 * @param bid The new bid to add
	 * @return true if the bid as added, false otherwise
	 */
	public boolean addBid(Auction auction, Bid bid) {

		List<Bid> bids = auction.getBids();

		bids.sort((bid1, bid2) -> Math.toIntExact(bid2.getAmount() - bid1.getAmount()));
		if (bids.get(0).getAmount() >= bid.getAmount())
			return false;

		bids.add(bid);

		context.createProducer().setProperty("topicUser", "createBid").send(topic, auction);

		return true;
	}


	public List<Bid> getMostRecent(Auction auction, int size) {

		List<Bid> bids = auction.getBids();

		bids.sort((bid1, bid2) -> Math.toIntExact(bid2.getAmount() - bid1.getAmount()));

		size = size > bids.size() ? bids.size() : size;

		return bids.subList(0, size);
	}


	@SuppressWarnings("unchecked")
	public List<Auction> getAllActiveAuctions() {

		Query query = em.createNamedQuery(Auction.QUERY_FIND_ACTIVE);
		query.setParameter("time", System.currentTimeMillis());
		return query.getResultList();
	}


	@SuppressWarnings("unchecked")
	public List<Auction> getActiveAuctions(int offset, int limit) {

		Query query = em.createNamedQuery(Auction.QUERY_FIND_ACTIVE);
		query.setParameter("time", System.currentTimeMillis());
		query.setFirstResult(offset);
		query.setMaxResults(limit);
		return query.getResultList();
	}


	@SuppressWarnings("unchecked")
	public int getActiveAuctionsSize() {

		Query query = em.createNamedQuery(Auction.QUERY_FIND_ACTIVE);
		query.setParameter("time", System.currentTimeMillis());
		return query.getResultList().size();
	}


	@SuppressWarnings("unchecked")
	public List<Auction> getAllAuctions() {

		Query query = em.createNamedQuery(Auction.QUERY_FIND_ALL);
		return query.getResultList();
	}


	public boolean checkIfActive(Auction auction) {
		String string = "";

		if (System.currentTimeMillis() < auction.getEndTime()) {
			string = "createBid";
		}else {
			string = "auctionWon";
		}
		context.createProducer().setProperty("topicUser", string).send(topic, auction);
		return true;
	}

}

