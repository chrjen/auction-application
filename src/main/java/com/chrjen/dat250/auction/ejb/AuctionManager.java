package com.chrjen.dat250.auction.ejb;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSSessionMode;
import javax.jms.Topic;

import com.chrjen.dat250.auction.entities.Auction;

@Stateless
public class AuctionManager {

	@EJB
	private AuctionDao auctionDao;

	Logger logger = Logger.getLogger(getClass().getName());

	@Inject
	@JMSConnectionFactory("jms/dat250/ConnectionFactory")
	@JMSSessionMode(JMSContext.AUTO_ACKNOWLEDGE)
	private JMSContext context;

	@Resource(lookup = "jms/dat250/Topic")
	private Topic topic;

	int auctionId = 0;
	@Schedule(second = "*/2",minute = "*", hour = "*")
	public void checkIfStillActive() {
		List <Auction> auctions = auctionDao.getAllActiveAuctions();
		int currentAuctionId = auctions.get(0).getId();

		if (currentAuctionId == auctionId) {
			if (System.currentTimeMillis() < auctions.get(0).getEndTime()) {
				logger.info("Auction " + auctions.get(0).getId() + " ends in " + (((auctions.get(0).getEndTime()-System.currentTimeMillis())/1000)/60)/60 +
						" hours and "+ (((auctions.get(0).getEndTime()-System.currentTimeMillis())/1000)/60)%60 + "minutes" );
			}else {
				logger.info("Auction " + auctions.get(0).getId() + " is finished ");
				auctionDao.checkIfActive(auctions.get(0));
			}
		}else {
			if (System.currentTimeMillis() < auctions.get(0).getEndTime()) {
				logger.info("Auction " + auctions.get(0).getId() + " ends in " + (((auctions.get(0).getEndTime()-System.currentTimeMillis())/1000)/60)/60 +
						" hours and "+ (((auctions.get(0).getEndTime()-System.currentTimeMillis())/1000)/60)%60 + "minutes" );
				auctionId = currentAuctionId;
			}else {
				logger.info("Auction " + auctionDao.getAuction(auctionId).getId() + " is finished ");
				auctionDao.checkIfActive(auctionDao.getAuction(auctionId));
			}
		}

	}






}
