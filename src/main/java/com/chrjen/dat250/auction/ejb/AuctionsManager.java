package com.chrjen.dat250.auction.ejb;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSSessionMode;
import javax.jms.Topic;

import com.chrjen.dat250.auction.entities.Auction;

@Stateless
public class AuctionsManager {

	@EJB
	private AuctionDao auctionDao;

	Logger logger = Logger.getLogger(getClass().getName());

	@Inject
	@JMSConnectionFactory("jms/dat250/ConnectionFactory")
	@JMSSessionMode(JMSContext.AUTO_ACKNOWLEDGE)
	private JMSContext context;

	@Resource(lookup = "jms/dat250/Topic")
	private Topic topic;

	int i = -1;
	@Schedule(second = "*/10",minute = "*", hour = "*")
	public void checkAll() {
		i++;
		List <Auction> auctions = auctionDao.getAllAuctions();
		auctionDao.checkIfActive(auctions.get(i));

		if(i == auctionDao.getAllAuctions().size()-1)
			i = -1;
	}

}