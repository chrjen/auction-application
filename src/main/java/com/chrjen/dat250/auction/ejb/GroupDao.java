package com.chrjen.dat250.auction.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.chrjen.dat250.auction.entities.Group;

@Stateless
@SuppressWarnings("WeakerAccess")
public class GroupDao {

	@PersistenceContext(unitName="AuctionPU")
	private EntityManager em;

	public Group findRoleByEmail(String email) {
		TypedQuery<Group> query = em.createNamedQuery("findRoleByEmail", Group.class);
		query.setParameter("email", email);
		Group group = null;
		try {
			group = query.getSingleResult();
		} catch (Exception e) {
			// getSingleResult throws NoResultException in case there is no user in DB
			// ignore exception and return NULL for user instead
		}
		return group;
	}
}
