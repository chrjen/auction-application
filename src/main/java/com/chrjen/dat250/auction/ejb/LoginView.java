package com.chrjen.dat250.auction.ejb;

import java.io.Serializable;
import java.security.Principal;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.chrjen.dat250.auction.entities.Group;
import com.chrjen.dat250.auction.entities.Person;


@Named(value = "loginView")
@SessionScoped
public class LoginView implements Serializable {

	private static final long serialVersionUID = 3254181235309041386L;
	private static final String LOGINFAILED = "loginFailed";
	private static final String USERLOGGEDIN = "auctions";
	private static final String ADMINLOGGEDIN = "auctions";
	private static final String LOGGEDINBUTNOROLE = "norole";
	//private static final String LOGOUT = "login";

	private static Logger log = Logger.getLogger(LoginView.class.getName());

	@Inject
	private PersonDao personDao;
	@Inject
	private GroupDao groupDao;
	private String email;
	private String password;
	private Person person;

	public String login() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

		try {
			request.login(email, password);
		} catch (ServletException e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", null));
			return LOGINFAILED;
		}

		Principal principal = request.getUserPrincipal();

		this.person = personDao.findPersonByEmail(principal.getName());
		log.info("Authentication done for user: " + principal.getName());

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		sessionMap.put("username", person);

		// Check role of user
		// get role
		Group group = groupDao.findRoleByEmail(email);
		String role = group.getGroupname();

		if (role.equals("persons")) {
			return USERLOGGEDIN;
		} else if (role.equals("admins")) {
			return ADMINLOGGEDIN;
		} else {
			return LOGGEDINBUTNOROLE;
		}

		/*
		// After user tries to login, redirect to these given pages
		if (request.isUserInRole("persons")) {
			return USERLOGGEDIN;
		} else if (request.isUserInRole("admins")) {
			return ADMINLOGGEDIN;
		} else {
			return LOGGEDINBUTNOROLE;
		}
		*/

		/*
		String role = request.getParameter("role");
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Role is " + role, null));
		return "";
		*/
	}

	public String logout() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

		try {
			this.person = null;
			request.logout();
			// clear the session
			((HttpSession) context.getExternalContext().getSession(false)).invalidate();
		} catch (ServletException e) {
			log.log(Level.SEVERE, "Failed to logout user!", e);
			return "tutLogin";
		}

//		return "/login?faces-redirect=true";
		return "login";
	}

	public Person getAuthenticatedUser() {
		return person;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}