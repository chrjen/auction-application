package com.chrjen.dat250.auction.ejb;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.chrjen.dat250.auction.entities.Address;
import com.chrjen.dat250.auction.entities.CreditCard;
import com.chrjen.dat250.auction.entities.Group;
import com.chrjen.dat250.auction.entities.Person;
import com.chrjen.dat250.auction.util.AuthenticationUtils;


@Stateless
@SuppressWarnings("WeakerAccess")
public class PersonDao {

	@PersistenceContext(unitName="AuctionPU")
	private EntityManager em;

	public void persist(Person person) {

		em.persist(person);
	}

	// Copied method - returns person, is not void. Something need doing?
	public Person createPerson(Person person) {
		try {
			person.setPassword(AuthenticationUtils.encodeSHA256(person.getPassword()));
		} catch (Exception e) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, e);
			e.printStackTrace();
		}
		Group group = new Group();
		group.setEmail(person.getEmail());
		group.setGroupname(Group.PERSONS_GROUP);
		em.persist(person);
		em.persist(group);

		return person;
	}

	public void persist(CreditCard creditCard) {

		em.persist(creditCard);
	}


	public void persist(Address address) {

		em.persist(address);
	}


	public Person getPerson(int id) {

		return em.find(Person.class, id);
	}

	// Method copied from security tutorial
	public Person findPersonByEmail(String email) {
		TypedQuery<Person> query = em.createNamedQuery("findPersonByEmail", Person.class);
		query.setParameter("email", email);
		Person person = null;
		try {
			person = query.getSingleResult();
		} catch (Exception e) {
			// getSingleResult throws NoResultException in case there is no user in DB
			// ignore exception and return NULL for user instead
		}
		return person;
	}

}
