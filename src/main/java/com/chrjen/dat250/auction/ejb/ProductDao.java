package com.chrjen.dat250.auction.ejb;

import com.chrjen.dat250.auction.entities.Product;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@SuppressWarnings("WeakerAccess")
public class ProductDao {

	@PersistenceContext(unitName="AuctionPU")
	private EntityManager em;


	public void persist(Product rating) {

		em.persist(rating);
	}


	public Product getProduct(int id) {

		return em.find(Product.class, id);
	}

}
