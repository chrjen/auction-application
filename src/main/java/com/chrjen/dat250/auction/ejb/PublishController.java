package com.chrjen.dat250.auction.ejb;

import com.chrjen.dat250.auction.entities.Auction;
import com.chrjen.dat250.auction.entities.Bid;
import com.chrjen.dat250.auction.entities.Product;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Named("publishController")
@SessionScoped
public class PublishController implements Serializable {

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@EJB
	private PersonDao personDao;
	@EJB
	private ProductDao productDao;
	@EJB
	private AuctionDao auctionDao;

	private String productName;
	private String productDescription;
	private String productImageUrl;
	private String productCategory;
	private String auctionAmount;
	private String auctionEndTime;


	public String save() {

		long endTime = 0;
		long amount = 0;

		try {
			Date endDate = dateFormat.parse(auctionEndTime);
			endTime = endDate.getTime();
			amount = Long.parseLong(auctionAmount);
		} catch (ParseException e) {
			return "publish";
		}

		Product product = new Product(productName, productDescription,
				productImageUrl, productCategory);
		productDao.persist(product);

		Auction auction = new Auction(0, endTime,
				personDao.getPerson(0), product);

		Bid bid = new Bid(amount);
		auctionDao.persist(bid);
		auctionDao.addBid(auction, bid);

		auctionDao.persist(auction);

		return "publish";
	}


	public String publish() {

		long endTime = 0;
		long amount = 0;

		try {
			Date endDate = dateFormat.parse(auctionEndTime);
			endTime = endDate.getTime();
			amount = Long.parseLong(auctionAmount.replace(".","").replace(",",""));
		} catch (ParseException e) {
			return "publish";
		}

		Product product = new Product(productName, productDescription,
				productImageUrl, productCategory);
		productDao.persist(product);

		Auction auction = new Auction(System.currentTimeMillis(), endTime,
				personDao.getPerson(0), product);

		Bid bid = new Bid(amount);
		auctionDao.persist(bid);
		auction.getBids().add(bid);

		auctionDao.persist(auction);

		return "publish";
	}


	public String getProductName() {

		return productName;
	}


	public void setProductName(String productName) {

		this.productName = productName;
	}


	public String getProductDescription() {

		return productDescription;
	}


	public void setProductDescription(String productDescription) {

		this.productDescription = productDescription;
	}


	public String getProductImageUrl() {

		return productImageUrl;
	}


	public void setProductImageUrl(String productImageUrl) {

		this.productImageUrl = productImageUrl;
	}


	public String getProductCategory() {

		return productCategory;
	}


	public void setProductCategory(String productCategory) {

		this.productCategory = productCategory;
	}


	public String getAuctionAmount() {

		return auctionAmount;
	}


	public void setAuctionAmount(String auctionAmount) {

		this.auctionAmount = auctionAmount;
	}


	public String getAuctionEndTime() {

		return auctionEndTime;
	}


	public void setAuctionEndTime(String auctionEndTime) {

		this.auctionEndTime = auctionEndTime;
	}
}
