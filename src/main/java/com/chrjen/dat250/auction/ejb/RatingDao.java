package com.chrjen.dat250.auction.ejb;

import com.chrjen.dat250.auction.entities.Rating;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


public class RatingDao {

	@PersistenceContext(unitName="AuctionPU")
	private EntityManager em;

	public void persist(Rating rating) {

		em.persist(rating);
	}


	public Rating getRating(int id) {

		return em.find(Rating.class, id);
	}


	@SuppressWarnings("unchecked")
	public List<Rating> getAllRatings() {

		return em.createNamedQuery(Rating.QUERY_FIND_ALL).getResultList();
	}


	@SuppressWarnings("unchecked")
	public List<Rating> getAllRatingsOnUser(int userId) {

		return em.createNamedQuery(Rating.QUERY_FIND_ALL_RATEE).getResultList();
	}


	@SuppressWarnings("unchecked")
	public List<Rating> getAllRatingsFromUser(int userId) {

		return em.createNamedQuery(Rating.QUERY_FIND_ALL_RATER).getResultList();
	}
}
