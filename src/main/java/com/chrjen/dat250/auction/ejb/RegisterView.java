package com.chrjen.dat250.auction.ejb;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;

import com.chrjen.dat250.auction.entities.Address;
import com.chrjen.dat250.auction.entities.Address.Country;
import com.chrjen.dat250.auction.entities.CreditCard;
import com.chrjen.dat250.auction.entities.Person;

@DeclareRoles({"admins", "persons"})
@PermitAll
@Named(value = "registerView")
@SessionScoped
public class RegisterView implements Serializable {
	private static final long serialVersionUID = 1685823449195612778L;
	private static Logger log = Logger.getLogger(RegisterView.class.getName());
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	// User info
	@Inject
	private PersonDao personDao;
	private String name;
	private String email;
	private String phoneNumber;
	private String password;
	private String confirmPassword;

	// Address info
	private String adrName = "Home"; // temp solution
	private String streetAdr;
	private String zipCode;
	private String city;
	private String country;

	// Creditcard info
	private String ccName = "Personal"; // temp solution
	private String ccNumbers;
	private String expTime;
	private String cvc;

	/*
	 * @return webpage to direct user to upon register completion
	 */
	public String register() {
		// Register personal info
		Person person = new Person(email, password, name, phoneNumber);
		personDao.createPerson(person);
		log.info("New user created with e-mail: " + email + " and name: " + name);

		// Register Address info
		Address address = new Address(adrName, streetAdr, zipCode, city, country);
		personDao.persist(address);
		person.getAddresses().add(address);

		// Register CC info
		long endTime = 0;

		try {
			Date endDate = dateFormat.parse(expTime);
			endTime = endDate.getTime();
		} catch (ParseException e) {
			return "login";
		}
		CreditCard creditCard = new CreditCard(ccName, ccNumbers, endTime, cvc);
		personDao.persist(creditCard);
		person.getCreditCards().add(creditCard);


		return "auctions";
	}

	public List<Country> getCountries(){
		return Address.COUNTRIES;
	}


	public void validatePassword(ComponentSystemEvent event) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		UIComponent components = event.getComponent();

		// get password
		UIInput uiInputPassword = (UIInput) components.findComponent("password");
		String password = uiInputPassword.getLocalValue() == null ? "" : uiInputPassword.getLocalValue().toString();
		String passwordId = uiInputPassword.getClientId();

		// get confirm password
		UIInput uiInputConfirmPassword = (UIInput) components.findComponent("confirmpassword");
		String confirmPassword = uiInputConfirmPassword.getLocalValue() == null ? ""
				: uiInputConfirmPassword.getLocalValue().toString();

		// Check password fields are not empty
		if (password.isEmpty() || confirmPassword.isEmpty()) {
			return;
		}
		// Check password matches confirm password
		if (!password.equals(confirmPassword)) {
			FacesMessage msg = new FacesMessage("Confirm password does not match password");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage(passwordId, msg);
			facesContext.renderResponse();
		}
		// Check user does not already exists
		if (personDao.findPersonByEmail(email) != null) {
			FacesMessage msg = new FacesMessage("User with this e-mail already exists");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			facesContext.addMessage(passwordId, msg);
			facesContext.renderResponse();
		}
	}

	// Getters and setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getStreetAdr() {
		return streetAdr;
	}

	public void setStreetAdr(String streetAdr) {
		this.streetAdr = streetAdr;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCcNumbers() {
		return ccNumbers;
	}

	public void setCcNumbers(String ccNumbers) {
		this.ccNumbers = ccNumbers;
	}

	public String getExpTime() {
		return expTime;
	}

	public void setExpTime(String expTime) {
		this.expTime = expTime;
	}

	public String getCvc() {
		return cvc;
	}

	public void setCvc(String cvc) {
		this.cvc = cvc;
	}
}
