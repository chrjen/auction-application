package com.chrjen.dat250.auction.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Entity
@XmlRootElement
@XmlType(propOrder = {
		"id",
		"name",
		"streeName",
		"zipCode",
		"city",
		"country",
})
public class Address implements Serializable {

	public static final List<String> COUNTRY_CODES = Arrays.asList(Locale.getISOCountries());
	public static final List<Country> COUNTRIES;

	static {
		COUNTRIES = new ArrayList<>();

		for (String code : COUNTRY_CODES) {
			String name = new Locale("", code).getDisplayName();
			COUNTRIES.add(new Country(code, name));
		}
	}

	@TableGenerator(
			name = "addressIdGenerator",
			allocationSize = 1,
			initialValue = 1000)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "addressIdGenerator")
	private int id;
	
	private String name; // User may add a name for his address, e.g. "Home", "Work", etc.
	
	private String streetName;
	
	private String zipCode;
	
	private String city;
	
	private String country;


	public Address() {
		// empty constructor
	}


	public Address(String name, String streetName, String zipCode, String city, String country) {

		if (!COUNTRY_CODES.contains(country))
			throw new IllegalArgumentException("Unknown country code: " + country);

		this.name = name;
		this.streetName = streetName;
		this.zipCode = zipCode;
		this.city = city;
		this.country = country;
	}


	@XmlElement
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {

		if (!COUNTRIES.contains(country))
			throw new IllegalArgumentException("Unknown country code: " + country);

		this.country = country;
	}


	public static class Country {

		public String code;
		public String name;


		public Country(String code, String name) {

			this.code = code;
			this.name = name;
		}
	}
}
