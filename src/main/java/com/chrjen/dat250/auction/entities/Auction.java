package com.chrjen.dat250.auction.entities;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@Entity
@XmlRootElement
@XmlType(propOrder = {
		"id",
		"publishTime",
		"publishDate",
		"endTime",
		"endDate",
		"product",
		"seller",
})
@NamedQuery(name = "auction.find_all", query = "SELECT a FROM Auction a ORDER BY a.id")
@NamedQuery(name = "auction.find_active",
		query = "SELECT a FROM Auction a WHERE :time < a.endTime ORDER BY a.endTime")
public class Auction implements Serializable {

	public static final String QUERY_FIND_ALL = "auction.find_all";
	public static final String QUERY_FIND_ACTIVE = "auction.find_active";

	@TableGenerator(
			name = "auctionIdGenerator",
			allocationSize = 1,
			initialValue = 1000)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "auctionIdGenerator")
	private int id;

	private long publishTime;
	private long endTime;

	private Person seller;
	private Product product;

	@OneToMany
	private List<Bid> bids = new ArrayList<>();


	public Auction() {
		// Empty constructor
	}


	public Auction(long publishTime, long endTime, Person seller, Product product) {

		this.publishTime = publishTime;
		this.endTime = endTime;
		this.seller = seller;
		this.product = product;
	}


	@XmlElement
	public int getId() {

		return id;
	}

	public long getPublishTime() {

		return publishTime;
	}


	@XmlElement
	public String getPublishDate() {

		return Instant.ofEpochMilli(publishTime).atOffset(ZoneOffset.UTC).toString();
	}


	public void setPublishTime(long publishTime) {

		if (this.publishTime != 0L)
			throw new IllegalStateException("Auction in progress cannot be edited");

		this.publishTime = publishTime;
	}


	public long getEndTime() {

		return endTime;
	}


	@XmlElement()
	public String getEndDate() {

		return Instant.ofEpochMilli(endTime).atOffset(ZoneOffset.UTC).toString();
	}


	public void setEndTime(long endTime) {

		if (this.publishTime != 0L)
			throw new IllegalStateException("Auction in progress cannot be edited");

		this.endTime = endTime;
	}


	public Product getProduct() {

		return product;
	}


	public void setProduct(Product product) {

		if (this.publishTime != 0L)
			throw new IllegalStateException("Auction in progress cannot be edited");

		this.product = product;
	}


	public Person getSeller() {

		return seller;
	}


	public void setSeller(Person seller) {

		if (this.publishTime != 0L)
			throw new IllegalStateException("Auction in progress cannot be edited");

		this.seller = seller;
	}


	public List<Bid> getBids() {
		//bids.sort((bid1, bid2) -> Math.toIntExact(bid2.getAmount() - bid1.getAmount()));
		return bids;
	}

	public Bid getHighestBid() {
		bids.sort((bid1, bid2) -> Math.toIntExact(bid2.getAmount() - bid1.getAmount()));
		return bids.get(0);
	}


}
