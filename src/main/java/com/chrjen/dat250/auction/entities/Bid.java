package com.chrjen.dat250.auction.entities;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneOffset;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@Entity
@XmlRootElement
@XmlType(propOrder = {
		"id",
		"time",
		"date",
		"amount",
		"bidder",
})
@NamedQuery(name="find_all_bids", query="SELECT b FROM Bid b ORDER BY b.id")
public class Bid implements Serializable {

	public static final String QUERY_FIND_ALL = "find_all_bids";
	//@Id
	//@GeneratedValue


	@TableGenerator(
			  name = "bidIdGenerator",
			  allocationSize = 1,
			  initialValue = 1000)
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE,generator="bidIdGenerator")
	private int id;

	private long time;
	private long amount;

	private Person bidder;


	public Bid() {
		// Empty constructor
	}


	public Bid(long amount) {

		this.amount = amount;
		this.time = System.currentTimeMillis();
	}


	public Bid(long amount, long time) {

		this.amount = amount;
		this.time = time;
	}


	@XmlElement
	public int getId() {

		return id;
	}


	public void setTime(long time) {

		this.time = time;
	}


	public long getTime() {

		return time;
	}


	@XmlElement
	public String getDate() {

		return Instant.ofEpochMilli(time).atOffset(ZoneOffset.UTC).toString();
	}


	public void setAmount(long amount) {

		this.amount = amount;
	}


	public long getAmount() {

		return amount;
	}


	public void setBidder(Person bidder) {

		this.bidder = bidder;
	}


	public Person getBidder() {

		return bidder;
	}

}