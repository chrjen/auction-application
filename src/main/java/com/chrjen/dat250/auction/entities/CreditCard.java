package com.chrjen.dat250.auction.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;


@Entity
@XmlRootElement
@XmlType(propOrder = {
		"id",
		"name",
		"ccnumber",
		"exptime",
		"cvc",
})
public class CreditCard implements Serializable {

	@TableGenerator(
			name = "creditCardIdGenerator",
			allocationSize = 1,
			initialValue = 1000)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "creditCardIdGenerator")
	private int id;

	private String name; 		// trenger vi aa la brukere lage navn paa cc?
	private String ccNumber;
	private long expTime;
	private String cvc;

	public CreditCard() {
		// Empty constructor
	}


	public CreditCard(String name, String ccNumber, long expTime, String cvc) {

		this.name = name;
		this.ccNumber = ccNumber;
		this.expTime = expTime;
		this.cvc = cvc;
	}


	@XmlElement
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCcNumber() {
		return ccNumber;
	}

	public void setCcNumber(String ccNumber) {
		// TODO: Lage en sjekk her? Evt "ett nivaa opp"?
		this.ccNumber = ccNumber;
	}


	@XmlTransient
	public long getExpTime() {

		return expTime;
	}


	@XmlElement(name = "expiration")
	public String getExpDate() {

		Date expDate = new Date(expTime);
		return String.format("%02d/%02d", expDate.getMonth() + 1, (expDate.getYear() % 100));
	}

	public void setExpTime(long expTime) {
		// TODO: Lage en sjekk om det er en dato, og om den er i fremtiden?
		this.expTime = expTime;
	}

	public String getCvc() {
		return cvc;
	}

	public void setCvc(String cvc) {
		// TODO: Lage enkel sjekk om det er tre tall? Eller om det ogsaa skal ett "nivaa opp"
		this.cvc = cvc;
	}




}
