package com.chrjen.dat250.auction.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


@Entity
@XmlRootElement
@XmlType(propOrder = {
		"id",
		"fullName",
		"email",
		"phoneNumber",
})
@NamedQueries({
	@NamedQuery(name = "findPersonByEmail", query = "SELECT p FROM Person p WHERE p.email = :email")
})
@Table(name="person")
public class Person implements Serializable {

	@TableGenerator(
			name = "personIdGenerator",
			allocationSize = 1,
			initialValue = 1000)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "personIdGenerator")
	private int id;

	@Column(name="email", nullable=false, length=255)
	private String email;

	@Column(name="password", nullable=false, length=64)
	private String password;

	private String fullName;
	private String phoneNumber;

	@OneToMany
	private List<Address> addresses = new ArrayList<>();

	@OneToMany
	private List<CreditCard> creditCards = new ArrayList<>();

	public Person() {
		// Empty constructor
	}

	public Person(String email, String password, String fullName, String phoneNumber) {
		this.email = email;
		this.password = password;
		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
	}

	@XmlElement
	public int getId() {
		return id;
	}

	@XmlTransient
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public List<CreditCard> getCreditCards() {
		return creditCards;
	}
}