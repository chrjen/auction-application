package com.chrjen.dat250.auction.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@Entity
@XmlRootElement
@XmlType(propOrder = {
		"id",
		"name",
		"category",
		"imageURL",
		"description",
		"specifications",
})
public class Product implements Serializable {

	@TableGenerator(
			name = "productIdGenerator",
			allocationSize = 1,
			initialValue = 1000)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "productIdGenerator")
	private int id;
	
	private String name;

	@Column(length = 30000)
	private String description;
	
	private String imageURL;
	
	private String category;

	@ElementCollection
	private Map<String, String> specifications = new HashMap<>();

	public Product() {
		//empty constructor
	}


	public Product(String name, String description, String imageURL, String category) {

		this.name = name;
		this.description = description;
		this.imageURL = imageURL;
		this.category = category;
	}


	@XmlElement
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	@XmlElement
	public Map<String, String> getSpecifications() {
		return specifications;
	}
}
