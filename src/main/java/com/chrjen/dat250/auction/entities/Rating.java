package com.chrjen.dat250.auction.entities;

import java.time.Instant;
import java.time.ZoneOffset;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@Entity
@XmlRootElement
@XmlType(propOrder = {
		"id",
		"time",
		"date",
		"rating",
		"comment",
		"rater",
		"ratee",
})
@NamedQuery(name = "rating.find_all", query = "SELECT r FROM Rating r")
@NamedQuery(name = "rating.find_all_ratee", query = "SELECT r FROM Rating r WHERE :uid = r.ratee")
@NamedQuery(name = "rating.find_all_rater", query = "SELECT r FROM Rating r WHERE :uid = r.rater")
public class Rating {

	public static final String QUERY_FIND_ALL = "rating.find_all";
	public static final String QUERY_FIND_ALL_RATEE = "rating.find_all_ratee";
	public static final String QUERY_FIND_ALL_RATER = "rating.find_all_rater";

	@TableGenerator(
			name = "ratingIdGenerator",
			allocationSize = 1,
			initialValue = 1000)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "ratingIdGenerator")
	private int id;

	private long time;
	private double rating;
	private String comment;

	private Person rater;
	private Person ratee;

	public Rating() {
		// Empty constructor
	}


	public Rating(long time, double rating, String comment, Person rater, Person ratee) {

		this.time = time;
		this.rating = rating;
		this.comment = comment;
		this.rater = rater;
		this.ratee = ratee;
	}


	@XmlElement
	public int getId() {
		return id;
	}


	public long getTime() {

		return time;
	}


	@XmlElement
	public String getDate() {

		return Instant.ofEpochMilli(time).atOffset(ZoneOffset.UTC).toString();
	}


	public void setTime(long time) {

		this.time = time;
	}


	public double getRating() {

		return rating;
	}


	public void setRating(double rating) {

		this.rating = rating;
	}


	public String getComment() {

		return comment;
	}


	public void setComment(String comment) {

		this.comment = comment;
	}


	public Person getRater() {

		return rater;
	}


	public void setRater(Person rater) {

		this.rater = rater;
	}


	public Person getRatee() {

		return ratee;
	}


	public void setRatee(Person ratee) {

		this.ratee = ratee;
	}
}
