package com.chrjen.dat250.auction.jms;

import java.io.IOException;
import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.chrjen.dat250.auction.entities.Auction;
import com.google.gson.JsonObject;

@MessageDriven(mappedName = "jms/dat250/Topic", activationConfig = {
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "topicUser = 'auctionWon'") })
public class AuctionIsWonListener implements MessageListener {

	@Override
	public void onMessage(Message message) {
		Logger logg = Logger.getLogger(getClass().getName());
		try {
			Auction auction = message.getBody(Auction.class);
			JsonObject json = new JsonObject();
			json.addProperty("Auction id", auction.getId());
			json.addProperty("Product name", auction.getProduct().getName());
			json.addProperty("Auction is", "FINISHED");

			Logger logger = Logger.getLogger(getClass().getName());
			logger.info("DTWEET Auction id: " + auction.getId());
			//logger.info("DTWEET JSON: " + json);

			try {
				DweetConnection dc = new DweetConnection(auction.getId());
				dc.publish(json);
			} catch (IOException e) {
				logger.info("DTWEET IOExeption was thrown");
				e.printStackTrace();
			}
		} catch (JMSException e1) {
			logg.info("DTWEET JMSExeption was thrown");
			e1.printStackTrace();
		}


	}

}
