package com.chrjen.dat250.auction.rest;

import java.net.URI;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.chrjen.dat250.auction.ejb.AuctionDao;
import com.chrjen.dat250.auction.ejb.PersonDao;
import com.chrjen.dat250.auction.entities.Auction;
import com.chrjen.dat250.auction.entities.Bid;
import com.chrjen.dat250.auction.entities.Person;


@Path("/auctions")
@Stateless
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class AuctionRestService {

	@EJB
	private AuctionDao auctionDao;
	@EJB
	private PersonDao personDao;
	@Context
	private UriInfo uriInfo;

	//GET <app-path>/rest/auctions - which should return a representation
	//with references to all current auctions ongoing in the system unless provided with
	//parameter active-only=false
	@GET
	public Response getAuctions(@DefaultValue("true") @QueryParam("active-only") String active) {

		boolean filterActive = Boolean.parseBoolean(active);
		List <Auction> auctions;

		if (filterActive) {
			auctions = auctionDao.getAllActiveAuctions();
		}
		else {
			auctions = auctionDao.getAllAuctions();
		}

		GenericEntity<List<Auction>> wrapper = new GenericEntity<List<Auction>>(auctions) {};
		return Response.ok(wrapper).build();
	}

	//GET <app-path>/rest/auctions/{id} - which should return a representation
	//of the auction identified by id
	@GET
	@Path("{id}")
	public Response getAuction(@PathParam("id") String id) {
		int idInt = Integer.parseInt(id);
		Auction auction = auctionDao.getAuction(idInt);
		if (auction == null)
			throw new NotFoundException();
		return Response.ok(auction).build();
	}

	//GET <app-path>/rest/auctions/{id}/bids/ - which should return a representation
	//with reference to all current bids in the auction identified by id
	@GET
	@Path("{id}/bids")
	public Response getAuctionBids(@PathParam("id") String id) {
		int idInt = Integer.parseInt(id);
		Auction auction = auctionDao.getAuction(idInt);
		if (auction == null)
			throw new NotFoundException();
		GenericEntity<List<Bid>> wrapper = new GenericEntity<List<Bid>>(auction.getBids()) {};
		return Response.ok(wrapper).build();//returns the bidsList of the auction?
	}


	//GET <app-path>/rest/auctions/{aid}/bids/{bid} - which should return a representation
	//of the given bid within the auction identified by aid
	@GET
	@Path("{aid}/bids/{bid}")
	public Response getAuctionBid(@PathParam("aid") String aid,@PathParam("bid") String id) {
		int aidInt = Integer.parseInt(aid);
		int idInt = Integer.parseInt(id)-1;
		Auction auction = auctionDao.getAuction(aidInt);
		if (auction == null)
			throw new NotFoundException();
		List<Bid> bids = auction.getBids();
		if (idInt < 0 || idInt >= bids.size())
			throw new NotFoundException();
		Bid bid = bids.get(idInt);
		return Response.ok(bid).build();
	}

	//POST <app-path>/rest/auctions/{id}/bids - which creates a bid with a specified amount
	//in the auction identified by id and returns a representation of the bid. The amount
	//should be contained in the payload of the request (or optionally as a query parameter).
	// EXAMPLE: http://localhost:8080/AuctionApplication/rest/auctions/3/bids?amount=80000&bidderId=4
	@POST
	@Path("{aid}/bids")
	public Response createAuctionBid(@PathParam("aid") String aid,@QueryParam("amount") String amount,@QueryParam("bidderId") String bidderId) throws InterruptedException {
		int aidInt = Integer.parseInt(aid);
		int amountInt = Integer.parseInt(amount);
		int bidderIdInt = Integer.parseInt(bidderId);
		boolean isHighestBid = false;

		Auction auction = auctionDao.getAuction(aidInt);
		Person bidder = personDao.getPerson(bidderIdInt);
		Bid bid = new Bid(amountInt,System.currentTimeMillis());
		bid.setBidder(bidder);

		if (bid.getAmount() > auction.getHighestBid().getAmount())
			isHighestBid = true;

		if (isHighestBid) {

			auctionDao.addBid(auction, bid);
			auctionDao.persist(bid);

			List <Bid> bids = auction.getBids();
			//returns with the id of the bid http://localhost:8080/AuctionApplication/rest/auctions/3/bids/153
			//URI bidUri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(bid.getId())).build();

			//returns with the correct Path to the newly created bid. http://localhost:8080/AuctionApplication/rest/auctions/3/bids/2
			URI bidUri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(bids.size())).build();
			return Response.created(bidUri).build();
		}
		else {

			throw new WebApplicationException("Bid amount too low");
		}


	}
}