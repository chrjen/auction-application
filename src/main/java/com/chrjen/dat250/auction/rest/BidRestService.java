package com.chrjen.dat250.auction.rest;

import com.chrjen.dat250.auction.entities.Bid;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;

@Stateless
@Path("/bids")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class BidRestService {

	@PersistenceContext(unitName = "AuctionPU")
	private EntityManager em;


	@GET
	public Response getBids() {
		TypedQuery<Bid> query = em.createQuery("SELECT b FROM Bid b", Bid.class);
		List<Bid> bids = query.getResultList();
		GenericEntity<List<Bid>> wrapper = new GenericEntity<List<Bid>>(bids) {};
		return Response.ok(wrapper).build();
	}


	@GET
	@Path("/{id}")
	public Response getBid(@PathParam("id") String id) {
		int idInt = Integer.parseInt(id);
		Bid bid = em.find(Bid.class, idInt);
		if (bid == null)
			throw new NotFoundException();
		return Response.ok(bid).build();
	}
}
