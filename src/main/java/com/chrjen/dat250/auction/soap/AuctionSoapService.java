package com.chrjen.dat250.auction.soap;

import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.transaction.Transactional;

import com.chrjen.dat250.auction.ejb.AuctionDao;
import com.chrjen.dat250.auction.ejb.PersonDao;
import com.chrjen.dat250.auction.entities.Auction;
import com.chrjen.dat250.auction.entities.Bid;
import com.chrjen.dat250.auction.entities.Person;

@WebService(endpointInterface = "com.chrjen.dat250.auction.soap.SoapService")
public class AuctionSoapService implements SoapService {

	@EJB
	private AuctionDao auctionDao;
	@EJB
	private PersonDao personDao;


	@Override
	public List<Auction> getActiveAuctions() {

		return auctionDao.getAllActiveAuctions();
	}


	@Override
	public Auction getAuctionId(@WebParam(name="auctionId")int id) {

		return auctionDao.getAuction(id);
	}


	@Override
	@Transactional
	@WebResult(name = "isHighestBid")
	public Boolean createAuctionBid(@WebParam(name="auctionId")int aid,@WebParam(name="amount")int amount,@WebParam(name="bidderId") int bidderId){
		boolean isHighestBid = false;
		Auction auction = auctionDao.getAuction(aid);
		Person bidder = personDao.getPerson(bidderId);
		Bid bid = new Bid(amount,System.currentTimeMillis());
		bid.setBidder(bidder);

		if (bid.getAmount() > auction.getHighestBid().getAmount())
			isHighestBid = true;

		if (isHighestBid) {
			auctionDao.persist(bid);
			auctionDao.addBid(auction, bid);
		}

		return isHighestBid;
	}
}
