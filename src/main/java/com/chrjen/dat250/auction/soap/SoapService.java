package com.chrjen.dat250.auction.soap;

import java.util.List;

import javax.jws.WebService;

import com.chrjen.dat250.auction.entities.Auction;

@WebService
public interface SoapService {

	public List<Auction> getActiveAuctions();
	public Auction getAuctionId(int id);
	public Boolean createAuctionBid(int id, int Amount, int bidderId);

}
