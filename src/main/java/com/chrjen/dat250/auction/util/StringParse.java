package com.chrjen.dat250.auction.util;

public class StringParse {


	public static int getIntOrDefault(String number, int defaultValue) {

		int value = defaultValue;

		try {
			value = Integer.parseInt(number);
		}
		catch (NumberFormatException e) {
			// Do nothing and return default value
		}

		return value;
	}
}
