package com.chrjen.dat250.auction.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@FacesValidator("currencyValidator")
public class CurrencyValidator implements Validator {

	private static final String CURRENCY_PATTERN = "^\\d+[.,]\\d{2}$";
	private static Pattern pattern = Pattern.compile(CURRENCY_PATTERN);
	private Matcher matcher;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		matcher = pattern.matcher(value.toString());
		if (!matcher.matches()) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Amount incorrectly formatted", null));
		}
	}
}
